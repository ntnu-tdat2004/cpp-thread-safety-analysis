# C++ thread safety analysis example

## Download and enable thread safety analysis
The `clang++` compiler must be used instead of `g++`:
```sh
git clone https://gitlab.com/ntnu-tdat2004/cpp-thread-safety-analysis
CXX=clang++ juci cpp-thread-safety-analysis
```
If you are using another IDE than juCi++, make sure the environment variable `CXX` is set to `clang++` before starting the IDE.

Note that it is not necessary to set the environment variable on MacOS since `clang++` is the default compiler.
