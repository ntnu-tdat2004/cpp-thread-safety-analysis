#include "mutex.hpp"
#include <iostream>
#include <thread>

using namespace std;

class Main { // Thread Safety Analysis only applies to classes
public:
  int sum GUARDED_BY(sum_mutex) = 0; // Extra annontation to restrict access to sum
  Mutex sum_mutex;

  Main() {
    thread t1([this] {
      for (int i = 0; i < 1000; i++) {
        LockGuard lock(sum_mutex); // Must lock the mutex to access sum
        ++sum;
      }
    });
    thread t2([this] {
      for (int i = 0; i < 1000; i++) {
        LockGuard lock(sum_mutex); // Must lock the mutex to access sum
        ++sum;
      }
    });

    t1.join();
    t2.join();

    LockGuard lock(sum_mutex); // Must lock the mutex to access sum
    cout << sum << endl;
  }
};

int main() {
  Main();
}
// Output: 2000
